#!/usr/bin/env bash

CARD=$(aplay -l |grep USB |grep -oP "card \d{1}")
DEVICE=$(aplay -l |grep USB |grep -oP "device \d{1}")
PATCH_FILE=/etc/alsa/conf.d/timilink.conf

echo Определена карта: $CARD
echo Номер устройства: $DEVICE 

echo Патчим файл $PATCH_FILE
sed -i -e "s/device ./$DEVICE/" $PATCH_FILE
sed -i -e "s/card ./$CARD/" $PATCH_FILE

echo Перезагружаем диллер-бекенд
service dialer-backend restart
cat  $PATCH_FILE
service dialer-backend status
echo Finish


